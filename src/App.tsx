import { useContext } from 'react';
import Controls from 'components/Controls';
import { appContext } from './components/AppContext';
import image from './assets/gh.svg';

const App = () => {
  const {
    input: {
      horizontal,
      vertical,
      blur,
      spread,
      opacity,
      shadowColor,
      boxColor,
      bgColor,
    },
  } = useContext(appContext);

  const copyText = () => {
    navigator.clipboard.writeText(`
      box-shadow: ${horizontal}px ${vertical}px ${spread}px ${blur}px rgba(0,0,0,${opacity});
     -webkit-box-shadow: ${horizontal}px ${vertical}px ${spread}px ${blur}px rgba(0,0,0,${opacity});
     -moz-box-shadow: ${horizontal}px ${vertical}px ${spread}px ${blur}px rgba(0,0,0,${opacity});
    `);
  };

  return (
    <section
      className="grid grid-cols-1 md:grid-cols-3 w-full h-full"
      style={{ background: bgColor }}
    >
      <a
        className="absolute bottom-5 right-5"
        href="https://github.com/Nicolas-alt/shadow-generator"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img src={image} alt="Logo Gh repo" />
      </a>
      <Controls />
      <div
        className="h-full w-full col-span-1 md:col-span-2 px-4 pb-5 pt-0 grid grid-rows-3 grid-cols-1"
        style={{ background: bgColor }}
      >
        {/* Box draw */}
        <div
          className="rounded-lg m-5 row-span-1"
          style={{
            background: boxColor,
            boxShadow: `${horizontal}px ${vertical}px ${spread}px ${blur}px ${shadowColor}`,
          }}
        ></div>
        <div className="h-1/2 w-full row-span-1 grid place-self-center">
          <code className="bg-indigo-300 rounded-2xl overflow-hidden flex flex-col items-center justify-center p-5">
            <p>
              box-shadow: {horizontal}px {vertical}px {spread}px {blur}px
              rgba(0,0,0,{opacity});
            </p>
            <p className="col-end-4 place-self-center">
              -webkit-box-shadow: {horizontal}px {vertical}px {spread}px {blur}
              px rgba(0,0,0,{opacity});
            </p>
            <p className="col-end-4 place-self-center">
              -moz-box-shadow: {horizontal}px {vertical}px {spread}px {blur}px
              rgba(0,0,0,{opacity});
            </p>
          </code>
        </div>
        <button
          onClick={copyText}
          className="m-2 px-3 w-60 h-20 rounded-md row-span-1 bg-green-200 hover:bg-indigo-300 transition-all duration-700 place-self-center text-gray-50"
        >
          Copy
        </button>
      </div>
    </section>
  );
};

export default App;
