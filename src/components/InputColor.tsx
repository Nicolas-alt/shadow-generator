import PropTypes from 'prop-types';
import { Props } from 'ts/types';

const InputColor = ({ id, name, value, inputName, handleChange }: Props) => {
  return (
    <div className="grid grid-cols-12 my-2 px-4">
      <label className="col-span-5 text-sm" htmlFor={id}>
        {name}
      </label>
      <input
        className="place-self-center col-span-3 cursor-pointer"
        id={id}
        type="color"
        value={value}
        name={inputName}
        onChange={handleChange}
      />
      <input
        className="col-span-4 col-end-13 px-4 bg-transparent border-2 border-red-200"
        id={id}
        type="text"
        value={value}
        name={inputName}
        onChange={handleChange}
      />
    </div>
  );
};

InputColor.propTypes = {
  name: PropTypes.string.isRequired,
};

export default InputColor;
