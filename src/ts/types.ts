export type Props = {
  id: string;
  name: string;
  value: string | number;
  inputName: string;
  handleChange: (state: any) => void;
};
