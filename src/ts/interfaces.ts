export interface inputPropertiesInterface {
  horizontal: number;
  vertical: number;
  blur: number;
  spread: number;
  opacity: number;
  shadowColor: string;
  boxColor: string;
  bgColor: string;
}
