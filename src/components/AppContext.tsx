import { ChangeEvent, createContext, ReactNode, useState } from 'react';
import { inputPropertiesInterface } from 'ts/interfaces';

type Props = {
  children: ReactNode;
};

const inputProperties: inputPropertiesInterface = {
  horizontal: 26,
  vertical: 24,
  blur: 0,
  spread: 13,
  opacity: 55,
  shadowColor: '#a8a4a4',
  boxColor: '#3e61a3',
  bgColor: '#FFFFFF',
};

interface contextInterface {
  input: inputPropertiesInterface;
  setInput: (state: any) => void;
  handleChange: (state: any) => void;
}

export let appContext: any;

const AppContext = ({ children }: Props) => {
  const [input, setInput] = useState(inputProperties);

  const handleChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setInput({
      ...input,
      [target.name]: target.value,
    });
  };

  appContext = createContext<contextInterface>({
    input,
    setInput,
    handleChange,
  });

  return (
    <appContext.Provider
      value={{
        input,
        setInput,
        handleChange,
      }}
    >
      {children}
    </appContext.Provider>
  );
};

export default AppContext;
