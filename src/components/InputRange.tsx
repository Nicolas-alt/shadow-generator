import PropTypes from 'prop-types';
import { Props } from 'ts/types';

const InputRange = ({ id, name, value, inputName, handleChange }: Props) => {
  return (
    <div className="flex flex-col items-center px-3 my-4">
      <div className="grid grid-cols-9 mb-2">
        <label htmlFor={id} className="mr-2 col-span-5 ">
          {name}
        </label>
        <input
          id={id}
          className="bg-transparent col-end-9 -mr-4 border-red-200 border-2 px-2"
          type="text"
          value={value}
          name={inputName}
          onChange={handleChange}
          min="-200"
          max="200"
        />
        <p className="ml-2 col-end-10 justify-self-end">px</p>
      </div>
      <input
        className="w-full px-3"
        type="range"
        value={value}
        name={inputName}
        onChange={handleChange}
        min="-200"
        max="200"
      />
    </div>
  );
};

InputRange.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

export default InputRange;
