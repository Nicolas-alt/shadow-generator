import { useContext } from 'react';
import InputColor from './InputColor';
import InputRange from './InputRange';
import { appContext } from './AppContext';

const Controls = () => {
  const { input, handleChange } = useContext(appContext);

  return (
    <div className="h-full col-span-1 md:col-span-1 flex flex-col justify-center items-center px-4 relative z-10">
      {/* Horizonal Length*/}
      <InputRange
        value={input.horizontal}
        id="horizontalLength"
        name="Horizontal Length"
        inputName="horizontal"
        handleChange={handleChange}
      />

      {/* Vertical Length*/}
      <InputRange
        value={input.vertical}
        id="verticalLength"
        name="Vertical Length"
        inputName="vertical"
        handleChange={handleChange}
      />

      {/* Blur radius */}
      <InputRange
        value={input.blur}
        id="blur"
        name="Blur Radius"
        inputName="blur"
        handleChange={handleChange}
      />

      {/* Spread Radius */}
      <InputRange
        value={input.spread}
        id="spread"
        name="Spread Radius"
        inputName="spread"
        handleChange={handleChange}
      />

      {/* Opacity */}
      <InputRange
        value={input.opacity}
        id="opacity"
        name="Opacity"
        inputName="opacity"
        handleChange={handleChange}
      />

      {/* Shadow Color */}
      <InputColor
        value={input.shadowColor}
        id="shadowColor"
        name="Shadow Color"
        inputName="shadowColor"
        handleChange={handleChange}
      />

      {/* Box Color */}
      <InputColor
        value={input.boxColor}
        id="boxColor"
        name="Box Color"
        inputName="boxColor"
        handleChange={handleChange}
      />

      {/* Background Color */}
      <InputColor
        value={input.bgColor}
        id="backgroundColor"
        name="Background Color"
        inputName="bgColor"
        handleChange={handleChange}
      />
    </div>
  );
};

export default Controls;
